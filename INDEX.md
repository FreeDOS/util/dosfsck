# DOSFSCK

Checks the consistency of DOS file systems and attempts to repair any errors

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## DOSFSCK.LSM

<table>
<tr><td>title</td><td>DOSFSCK</td></tr>
<tr><td>version</td><td>2.11d</td></tr>
<tr><td>entered&nbsp;date</td><td>2012-01-08</td></tr>
<tr><td>description</td><td>Check the consistency of file system</td></tr>
<tr><td>summary</td><td>Checks the consistency of DOS file systems and attempts to repair any errors</td></tr>
<tr><td>keywords</td><td>scandisk,chkdsk</td></tr>
<tr><td>author</td><td>Werner Almesberger, Roman Hodek</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer</td></tr>
<tr><td>primary&nbsp;site</td><td>http://ericauer.cosmodata.virtuaserver.com.br/soft/by-others/</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/chkdsk/dosfsck/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
</table>
